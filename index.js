const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");

require("./src/db")
const routes = require("./src/routes");

const app = express();

app.use(cors());
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("api/v1/",routes)

app.use(function(err, req, res) {
  res.status(err.code).send(err.message);
})

app.listen(8000, (err) => {
  if (err) console.err("Server error!");
  console.info("Server is running on Port 8000");
});
