const router = require("express").Router()

const todoController = require("../api/controllers/todoController")
const auth = require("../api/middlewares/Auth")

router.get("todo",  todoController.getTodoInfo)
router.post("todo/add", todoController.add)
router.patch("todo/update", todoController.update)
router.delete("todo/delete", todoController.delete)

module.exports = router