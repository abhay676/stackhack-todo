const express = require("express");

const userRoutes = require("./userRoutes");
const todoRoutes = require("./todosRoutes");

const app = express();

app.use(userRoutes);
app.use(todoRoutes);

module.exports = app;
