## Routes

This folder contains files related to routes i.e `Todos routes` and `User routes`

#### Todos routes: `todosRoutes.js`

Todos routes consits following `end-points`: 
- `GET` /todo : Responsible for fetching the Todo details
- `POST` /todo/add :  Add todo to the DB
- `PATCH` /todo/update : Update the Todo by using query params
- `DELETE` /todo/delete : Delete Todo by using query params

#### User routes: `userRoutes.js`

- `POST` /login : Handle Login 
- `POST` /register : Handler new User registeration
- `GET` /user : Fetch all the user info
- `POST` /user/add : Add new user
- `PATCH` /user/update : Update user by using query params
- `DELETE` /user/delete : Delete user by using query params
