const router = require("express").Router()
const userController = require("../api/controllers/userController")

router.get("user", userController.getUserInfo)
router.post("login", userController.login)
router.post("register", userController.register)
router.patch("user/update", userController.update)
router.delete("user/delete", userController.delete)

module.exports = router