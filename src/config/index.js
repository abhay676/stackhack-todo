require("dotenv").config()

module.exports = {
    MONGODB_URL: process.env.MONGODB_URL,
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
    GENERATE_SALT: process.env.GENERATE_SALT
}