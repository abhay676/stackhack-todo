const jwt = require("jsonwebtoken")
const User = require("../models/User")
const config = require("../../config")

module.exports = async (req, res, next) => {
    const bearerHeader =
      req.headers["x-access-token"] || req.headers.authorization;
    if (!bearerHeader) {
      throw new ErrorHandler(401, "Token is not found")
    }
    try {
      const token = bearerHeader.replace("Bearer ", "");
      const verify = await jwt.verify(token, config.JWT_SECRET_KEY);
      const user = await User.findById(verify_id);
      req.id = verify._id;
      req.user = user;
      next();
    } catch (error) {
      res
        .status(401)
        .send({ success: false, code: 401, error: "Not Authorized" });
    }
  };
  