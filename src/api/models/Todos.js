const mongoose = require("mongoose")

const todoSchema = new mongoose.Schema({
    name: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    due_date: {
        type: mongoose.SchemaTypes.Date
    },
    status: {
        type: mongoose.SchemaTypes.String,
        required: true
    },
    label: {
        type: mongoose.SchemaTypes.String
    },
    priority: {
        type: mongoose.SchemaTypes.String
    }
},
{
    timestamps: true
})

const Todo = mongoose.model("Todos", todoSchema)

module.exports = Todo