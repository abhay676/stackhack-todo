const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const config = require("../../config");

const userSchema = new mongoose.Schema(
  {
    name: {
      type: mongoose.SchemaTypes.String,
      required: true,
    },
    email: {
      type: mongoose.SchemaTypes.String,
      required: true,
    },
    password: {
      type: mongoose.SchemaTypes.String,
      required: true,
    },
    tokens: [
      {
        token: {
          type: mongoose.SchemaTypes.String,
          required: true
        }
      }
    ]
  },
  { timestamps: true }
);

userSchema.pre("save", async function (next) {
  this.password = await bcrypt.hash(this.password, config.GENERATE_SALT);
  next();
});

userSchema.methods.comparePwd = async function (id, pwd) {
  const user = await User.findById(id);
  const cmpPwd = await bcrypt.compare(pwd, user.password);
  if (!cmpPwd) return false;
  return true;
};

userSchema.methods.generateToken = async function() {
  const user = this;
  const token = await jwt.sign({ _id: user._id.toString() }, config.JWT_SECRET_KEY);
  user.tokens = user.tokens.concat({ token });
  await user.save()
  return token
};


const User = mongoose.model("User", userSchema);

module.exports = User;
