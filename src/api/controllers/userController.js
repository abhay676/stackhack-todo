const User = require("../models/User")
const {ErrorHandler, Responsehandler} = require("../../utils/Handler")

exports.getUserInfo = async(req, res, next) => {
    try {
        res.send(Responsehandler(req.user))        
    } catch (error) {
        error.code = 404
        next(error)
    }
}

exports.register = async(req, res, next) => {
    try {
        const user = new User(req.body)
        const savedUser = await user.save()
        savedUser.generateToken()
        res.send(Responsehandler(savedUser))
    } catch (error) {
        next(error)
    }
}

exports.login = async(req, res, next) => {
    try {
        const {email, password} = req.body
        const user = await User.find({email})
        if(!user) throw new ErrorHandler(404, "Email is not registered")
        const pwd = await User.comparePwd(req.id, password)
        if(!pwd) throw new ErrorHandler(404, "Password is incorrect")
        user.generateToken()
        res.send(Responsehandler(user))
    } catch (error) {
        error.code = 404
        next(error)
    }
}

exports.update = async(req, res, next) => {
    try {
        const updateUser = await User.findOneAndUpdate({_id: req.id}, {...req.body})
        res.send(Responsehandler(updateUser))
    } catch (error) {
        error.code = 403
        next(error)
    }
}

exports.delete = async(req, res, next) => {
    try {
        await User.findOneAndDelete({_id: req.id})
        res.send(Responsehandler("Deleted successfully"))
    } catch (error) {
        error.code = 403
        next(error)
    }
}