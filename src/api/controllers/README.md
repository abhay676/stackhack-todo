## Controller

This folder contains files related to controller, which is responsible for manipulate db data.

This folder contains `two files`:  
1. **todoContoller.js** : This file contains all the functions related to `Todos actions`.
2. **userController.js** : This file contains all the functions related to `User's actions`.