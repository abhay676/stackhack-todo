const Todo  = require("../models/Todos")
const {ErrorHandler, Responsehandler} = require("../../utils/Handler")
exports.getTodoInfo = async(req, res, next) => {
    try {
        const id = req.query.id
        const todo = await Todo.findOne({_id: id})
        if(!todo) throw new ErrorHandler(404, "No task found")
        res.send(Responsehandler(toddo))
    } catch (error) {
        error.code = 404;
        next(error)
    }
}

exports.add = async( req, res, next) => {
    try {
    const todo = new Todo(req.body)
    const savedTodo = await todo.save()
    res.send(Responsehandler(savedTodo))
    } catch (error) {
        next(error)
    }
}

exports.update = async(req, res, next) => {
    try {
        const id = req.query.id
        const updateTodo = await Todo.findOneAndUpdate({_id: id}, {...req.body})
        if(!updateTodo) throw new ErrorHandler(404, "No task found to update")
        res.send(Responsehandler(updateTodo))
    } catch (error) {
        error.code = 403;
        next(error)
    }
}

exports.delete = async(req, res, next) => {
    try {
       const id = req.query.id
       const td = await Todo.findOneAndDelete({_id: id})
       if(!td) throw new ErrorHandler(404, "No task found to delete")
       res.send(Responsehandler("Deleted successfully"))
    } catch (error) {
        error.code = 404;
        next(error)
    }
}