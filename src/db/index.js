const mongoose = require("mongoose");

//TODO: update with mongoDB cloud url
module.exports = mongoose.connect(`mongodb://localhost:27017/stackhack`, {
  useNewUrlParser: true,
  useFindAndModify: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});
